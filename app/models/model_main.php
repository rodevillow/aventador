<?php

/**
 * Class Model_main
 */
Class Model_main Extends Model
{
    /**
     * Get List Names, Phones, Descriptions
     * @return array
     */
    function get_list(){
        return $this->db->query('SELECT * FROM list ORDER BY id DESC');
    }

    /**
     * Insert Into List
     */
    function insert_into_list($name, $phone, $description){
        $this->db->query("INSERT INTO list (name,phone,description) 
        VALUES ('".$name."','".$phone."','".$description."')");
    }

    /**
     * Insert Into List
     */
    function delete_row($id){
        $this->db->query("DELETE FROM list WHERE id=" . $id);
    }
}