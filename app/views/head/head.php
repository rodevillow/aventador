<head>
    <meta charset="UTF-8">
    <!--Tittle-->
    <title>PhoneBook</title>
    <!--Include Bootstrap-->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" type="text/css">
    <!--Include custom css-->
    <link rel="stylesheet" href="../../css/style.css" type="text/css">
    <!--Google JQ-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <!--Custom JS-->
    <script src="../../js/script.js"></script>
</head>