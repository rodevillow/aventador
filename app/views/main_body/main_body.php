<body>
    <div class="container">
        <div class="row">
            <div class="mid">
                <table id="myTable" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <td id="add-name" class="inputs">Name</td>
                        <td id="add-phone" class="inputs">Phone Number</td>
                        <td id="add-description" class="inputs">Description</td>
                        <td id="add-submit">
                            <button type="button" class="btn btn-success" id="add">Add</button>
                            <span class="glyphicon glyphicon-remove" id="remove-ico-in-add"></span>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($data as $tr){ ?>
                        <tr id="<?php echo $tr['id']; ?>">
                            <td><?php echo $tr['name']; ?></td>
                            <td><?php echo $tr['phone']; ?></td>
                            <td><?php echo $tr['description']; ?></td>
                            <td>
                                <span class="glyphicon glyphicon-paperclip edit"></span>
                                <span class="glyphicon glyphicon-remove remove"></span>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>