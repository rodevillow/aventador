<?php

/**
 * Class Route
 */
class Route
{
    /**
     * Start
     */
    static function start()
    {
        $controller_name = 'main';
        $action_name = 'index';

        if ($get_start = strripos($_SERVER['REQUEST_URI'],'?')){
            $clear_uri = substr($_SERVER['REQUEST_URI'],0,$get_start);
        }
        else{
            $clear_uri = $_SERVER['REQUEST_URI'];
        }

        $routes = explode('/', $clear_uri);

        if ( !empty($routes[1]) ){
            $controller_name = $routes[1];
        }

        if ( !empty($routes[2]) ){
            $action_name = $routes[2];
        }

        $model_name = 'Model_'.$controller_name;
        $controller_name = 'Controller_'.$controller_name;
        $action_name = 'action_'.$action_name;


        $model_file = strtolower($model_name).'.php';
        $model_path = "app/models/".$model_file;
        if(file_exists($model_path)){
            include "app/models/".$model_file;
        }

        $controller_file = strtolower($controller_name).'.php';
        $controller_path = "app/controllers/".$controller_file;
        if(file_exists($controller_path))
        {
            include "app/controllers/".$controller_file;
        }
        else
        {
            if ($controller_name == 'Controller_administrator')
                Route::ErrorPage404_admin();
            else
                Route::ErrorPage404();
        }

        $controller = new $controller_name;
        $action = $action_name;

        if(method_exists($controller, $action))
        {
            $controller->$action();
        }
        else
        {
            if ($controller_name == 'Controller_administrator')
                Route::ErrorPage404_admin();
            else
                Route::ErrorPage404();
        }

    }

    /**
     * Error Page 404
     */
    static function ErrorPage404()
    {
        echo "404 ERROR";
//        include 'app/controllers/controller_404.php';
//        $controller = new Controller_404;
//        $controller->action_index();
    }

    /**
     * Error Page 404 Admin
     */
    static function ErrorPage404_admin()
    {
        echo "404 ERROR ADMIN";
//        include 'app/controllers/controller_404.php';
//        $controller = new Controller_404;
//        $controller->action_admin();
    }
}