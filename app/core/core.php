<?php

/**
 * Class Core
 */
class Core {

    /**
     * Start Core
     * @return void
     */
    static function start() {
        require_once dirname(__FILE__).'/db.php';
        require_once dirname(__FILE__).'/model.php';
        require_once dirname(__FILE__).'/view.php';
        require_once dirname(__FILE__).'/controller.php';
        require_once dirname(__FILE__).'/router.php';

        Route::start();
    }
}