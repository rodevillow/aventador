<?php

/**
 * Class View
 */
class View
{
    /**
     * @var string
     */
    public $template_view;

    /**
     * @param string $content_view
     * @param null $template_view
     * @param null $data
     * @return void
     */
    function generate($content_view, $template_view = null, $data = null)
    {

        if ($template_view)
            include 'app/views/'.$template_view;
        else
            include 'app/views/'.$content_view;
    }
}