<?php

/**
 * Class Model
 */
class Model
{
    /**
     * @var db
     */
    protected $db;

    /**
     * Model constructor.
     */
    function __construct() {
        $this->db = new db;
    }
}