<?php

/**
 * Class Controller
 */
class Controller {
    /**
     * @var Model obj
     */
    public $model;

    /**
     * @var View obj
     */
    public $view;

    /**
     * @var string
     */
    protected $template_view = 'template_view.php';

    /**
     * Controller constructor.
     */
    function __construct()
    {
        $this->model = new Model;
        $this->view = new View;
    }

    /**
     * @param $url string
     * @return void
     */
    protected function redirect($url) {
        header("Location: http://".SITE_PATH."/$url");
        exit();
    }

    /**
     * Action Index
     * @return void
     */
    function action_index()
    {

    }
}