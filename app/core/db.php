<?php

/**
 * Class Data Base
 */
class db
{
    /**
     * @var PDO
     */
    private $connection;

    /**
     * @var string
     */
    public $query;

    /**
     * db constructor.
     */
    function __construct() {
        $user = 'root';
        $pass = 'root';

        try {
            $this->connection = new PDO('mysql:host=localhost;dbname=test', $user, $pass);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /**
     * Query and FETCH
     * @param $q string
     * @return array
     */
    function query($q) {
        $this->query = $this->connection->query($q);
        return $this->query = $this->query->fetchAll(PDO::FETCH_ASSOC);
    }
}