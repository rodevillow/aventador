<?php

/**
 * Class Controller_main
 */
Class Controller_main Extends Controller
{
    /**
     * Controller_main constructor.
     */
    function __construct() {
        $this->view = new View;
        $this->model = new Model_main;
    }

    /**
     * Action Index
     * @return void
     */
    function action_index() {
        $data = $this->model->get_list();
        $this->view->generate('main_view.php',NULL,$data);
    }

    /**
     * Action Insert
     * @return void
     */
    function action_insert(){
        $this->model->insert_into_list($_POST['name'], $_POST['phone'], $_POST['description']);
    }

    /**
     * Action Insert
     * @return void
     */
    function action_delete(){
        $this->model->delete_row($_POST['id']);
    }
}