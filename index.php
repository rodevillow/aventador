<?php

// Errors
ini_set('display_errors', 1);

// Include Core
require_once dirname(__FILE__).'/app/core/core.php';

// Core Start
Core::start();