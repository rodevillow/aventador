$(document).ready(function(){
    // If Add click, change head table to inputs add
    $('#add').click(function () {
        if($('#add-name').html() == '<input type="text" placeholder="Name">'){
            ajax_to_insert(getInputValue('#add-name'), getInputValue('#add-phone'), getInputValue('#add-description'));
            change_add_to_default();
            location.reload()
        }else{
            change_add_to_insert();
        }
    });

    // Back to default (Add menu)
    $('#remove-ico-in-add').click(function () {
        change_add_to_default();
    });

    // Options
    // Edit <tr> by id
    $('.edit').click(function () {
        alert('edit');
    });

    // Delete <tr> by id
    $('.remove').click(function () {
        // Get parent id for remove
        var id = $(this).parent().parent().attr('id');
        // Delete Ajax
        ajax_to_Delete(id);
        // Delete Frontend
        $('#' + id).remove();
    });я
    
});

// Get Input Value
function getInputValue(text) {
    return $(text).find('input').val();
}

// Input Text
function inputTxt(text) {
    return '<input type="text" placeholder="' + text + '">';
}

// Ajax To Delete Row
function ajax_to_Delete(id) {
    $.ajax({
        method: "POST",
        url: "main/delete",
        data: { id: id }
    })
        .done(function( msg ) {
        });
}
// Ajax To Insert Row

function ajax_to_insert(name, phone, description) {
    $.ajax({
        method: "POST",
        url: "main/insert",
        data: { name: name, phone: phone, description: description }
    })
        .done(function( msg ) {
        });
}

// Change Add To Insert
function change_add_to_insert() {
    $('#add-name').html(inputTxt('Name'));
    $('#add-phone').html(inputTxt('Phone'));
    $('#add-description').html(inputTxt('Description'));
    $('#remove-ico-in-add').css("display","block")
}

// Change Add To Default
function change_add_to_default() {
    $('#add-name').html('Name');
    $('#add-phone').html('Phone');
    $('#add-description').html('Description');
    $('#remove-ico-in-add').css("display","none")
}